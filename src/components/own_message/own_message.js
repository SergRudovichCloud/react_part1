import React, { Component } from 'react';
import { MessageItem } from '../index';
import './own_message.scss';

export default class OwnMessage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isEdit: false,
            value: this.props.message.text
        };
    }

    handleEdit = () => {
        if (this.state.isEdit) {
            this.setState({ isEdit: false })
        } else {
            this.setState({ isEdit: true })
        }
    }

    handleDelete = () => {
        this.props.onMessageDelete(this.props.message.id);
    }

    handleSend = () => {
        this.setState({ isEdit: false })
        this.props.onMessageEdit(this.props.message.id, this.state.value);
    }
    handleChange = (event) => {
        this.setState({ value: event.target.value });
    }

    render() {
        if (!this.state.isEdit) {
            return (
                <div className="own-message">
                    <MessageItem message={this.props.message} />
                    <div className="controls">
                        <button
                            className="message-edit"
                            onClick={this.handleEdit}
                        >Edit</button>
                        <button
                            className="message-delete"
                            onClick={this.handleDelete}
                        >Delete</button>
                    </div>

                </div>
            )
        } else {
            return (
                <div className="message-edit">
                    <textarea
                        className="message-edit-text"
                        value={this.state.value}
                        onChange={this.handleChange}
                    ></textarea>
                    <button
                        className="message-edit-button"
                        onClick={this.handleSend}
                    >Send</button>
                </div>
            )
        }

    }
}

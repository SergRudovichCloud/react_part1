import React, { Component } from 'react';
import { USER_ID, USER_NAME } from '../../config';
import './chat.scss';
import { getUrl } from '../../helpers/';
import {
    Preloader,
    MessageInput,
    MessageList,
    Header
} from '../index';

export default class Chat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allMessages: [],
            isDataLoad: false,
            messages: 0,
            users: 0,
            lastMessageData: '',
            userMesgCount: 0
        };
    }

    getData = (data) => {
        const fetchUsers = new Set();
        data.forEach(message => {
            fetchUsers.add(message.userId);
        });
        this.setState({
            allMessages: data,
            isDataLoad: true,
            messages: data.length,
            users: fetchUsers.size,
            lastMessageData: data[data.length - 1]?.createdAt
        })
    }

    componentDidMount() {
        getUrl(this.props.url, this.getData);
    }
    handleMessageSend = (messageTxt) => {
        const newMessage = {
            id: USER_ID + new Date(),
            userId: USER_ID,
            avatar: "",
            user: USER_NAME,
            text: messageTxt,
            createdAt: Date.now(),
            editedAt: ""
        }
        let newUser = 0;
        if (this.state.userMesgCount === 0) newUser += 1;
        this.setState(presState => ({
            allMessages: [...presState.allMessages, newMessage],
            messages: presState.messages + 1,
            users: presState.users + newUser,
            lastMessageData: newMessage.createdAt,
            userMesgCount: presState.userMesgCount + 1
        }))
    }

    handleMessageDelete = (id) => {
        const result = this.state.allMessages.filter(message => message.id !== id);
        let newUser = 0;
        if (this.state.userMesgCount === 1) newUser = -1;
        this.setState(presState => ({
            allMessages: [...result],
            messages: presState.messages - 1,
            users: presState.users + newUser,
            userMesgCount: presState.userMesgCount - 1
        }))
    }

    handleMessageEdit = (id, newText) => {
        const index = this.state.allMessages.findIndex(message => message.id === id);
        const result = [...this.state.allMessages];
        const editedMsg = result[index];
        editedMsg.text = newText;
        editedMsg.editedAt = Date.now();
        result.splice(index, 1);
        result.push(editedMsg);
        this.setState({
            allMessages: [...result],
        })
    }

    render() {

        if (this.state.isDataLoad) {
            return (
                <div className="chat">
                    {/* <div className='logo'>Logo</div> */}
                    <Header
                        users={this.state.users}
                        messages={this.state.messages}
                        lastMessageData={this.state.lastMessageData}
                    />
                    <MessageList
                        messages={this.state.allMessages}
                        onMessageDelete={this.handleMessageDelete}
                        onMessageEdit={this.handleMessageEdit}
                    />
                    <MessageInput
                        onMessageSend={this.handleMessageSend}
                    />
                </div>
            )
        } else {
            return (
                <Preloader />
            )
        }

    }
}

import React, { Component } from 'react';
import { OwnMessage, Message } from '../index';
import { USER_ID } from '../../config';
import './message_list.scss';

export default class MessageList extends Component {
    render() {
        return (
            <div className="message-list">
                {this.props.messages.map(message => {
                    // <span className="messages-divider">Data</span>
                    if (message.userId === USER_ID) {
                        return <OwnMessage 
                        key={message.id}
                            message={message}
                            onMessageDelete={this.props.onMessageDelete}
                            onMessageEdit={this.props.onMessageEdit}
                        />
                    } else {
                        return <Message key={message.id}
                            message={message}
                        />
                    }

                })}
            </div>
        )
    }
}

import React, { Component } from 'react';
import './preloader.scss';

export default class Preloader extends Component {
    render() {
        return (
            <div className="preloader">
                <span>I am loading...</span>
            </div>
        )
    }
}

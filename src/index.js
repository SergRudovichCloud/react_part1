import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.scss';
import Chat from './bsa';
import { MESSAGE_URL } from './config';

ReactDOM.render(
  <React.StrictMode>
    <Chat url={MESSAGE_URL}/>
  </React.StrictMode>,
  document.getElementById('root')
);

